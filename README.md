# Why Chevron?
Have you ever built software applications that had the requirement of sending a textual notification with dynamic content either by *Email*, *SMS* or
instant messaging services such as *Whatsapp*, *Telegram*, *Slack*  or even Facebook *Messenger*?
Every time we find ourselves in this situation, regardless of how our notification is expected to be delivered, we face this one 
common challenge: __How to generate the content in a decent manager instead of just hardcoding everything__? 
This is definitely a problem for template engines such as *Freemaker*, *JTwig* or *Apache Velocity*. They surely fix the hardcoding issue, but there are 
other issues/questions down the road that also need to be handled.
Chevron was created precisely to deal with those issues. It provides a simple API that has just enough abstractions for you to:
* Load templates from anywhere (Filesystem, classpath, database, in-memory, remote location, anywhere)
* Re-use templates, including one into another
* Defining validation rules for the variables used in templates
* Add support for new template engines over time
* Use multiple and different template engines at the same time for different templates
* Have the same template in multiple idioms and being able to select the correct template to use.

Congratulations. You now understand why we created __Chevron__.
Let's now look at the inners of it.

# Chevron design
Chevron is made of 6 core concepts:
* Template
* Template Engine
* Templates Pool
* Object Schema Pool
* Context
* Composer

## Template
A pattern used to generate dynamic content. A template has the following attributes:
* name
* format
* language
* schema
* qualifiers
* source

### name
A textual expression that uncovers the communication content of the template. 
Example: ACCOUNT-STATEMENT-EMAIL. 

### format
Defines the structure of the Template content. There are two possible formats:
* Text-Plain
* HTML

### language
It defines the notation used to manipulate and generate content dynamically.
Examples: freemaker, velocity, jsp.

### schema
Defines the template data requirements. The required parameter values that must be provided so that the template
can successfully generated content, the format of each parameter. The role of the Schema is not to expose
this information, instead it is to validate parameters.

### qualifier
A keyword used to represent a quality that distinguishes a template from others that communicate the same thing and therefore have the same name, but employing a different grammar (expressions, semantics, terms) and or idiom. 
If for example we want to have one template in two different Idioms (English and Portuguese), we could use the Idiom names as qualifiers. So we would have one template with the ENGLISH qualifier and another template with the PORTUGUESE qualifier. 
Also a single template can have multiple qualifiers. But qualifiers are way beyond Idiom. A very good way of proving that is imagining a scenario in which we want to have different versions of template, per target audience. 
We could have qualifier like: FORMAL_LANGUAGE, INFORMAL_LANGUAGE, VERY_FORMAL_LANGUAGE, RELAXED_LANGUAGE. 

### source
The actual content of the template.


## Template engine
Component that is responsible for merging a template with a set of parameters, executing all the content
generation expressions used in the template, therefore, a template engine implements a templating language.


## Templates Pool
A pool of Templates. This is where the Composer is supposed to fetch templates from. A template fetched
from this pool will only have a reference of a Schema and this reference must be resolved using
an __Object Schemas Pool__.

## Object Schemas Pool
A pool of Schemas. This is where the Composer is supposed to fetch Schemas from.


## Context
It aggregates all the components needed by the Composer to generate content: __Template engines__,  __Templates-Pool__, __Object-schemas-Pool__.
The context also contains a bunch of parameters that are expected to be automatically set when generating content from any template.


## Composer
It does a simple job: it generates dynamic content from a template.
For the composer to generate content, it requires the following inputs:
* Context
* template name
* template format
* template qualifiers (list)
* parameters (key-value)
