package org.chevron.model;

import org.chevron.model.schema.ObjectSchema;

import java.util.Optional;

public interface ObjectSchemasPool {

    Optional<ObjectSchema> getSchema(String name);

}
