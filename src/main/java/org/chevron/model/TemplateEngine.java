package org.chevron.model;

import java.util.Map;

public interface TemplateEngine<Config> {

    String getLanguage();
    TemplatingContext createContext(Config configuration, Map<String,Object> data);

}
