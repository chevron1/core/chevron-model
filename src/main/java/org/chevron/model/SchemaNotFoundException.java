package org.chevron.model;

public class SchemaNotFoundException extends ChevronException {

    private String name;

    public SchemaNotFoundException(String name) {
        super(String.format("Object schema not found: [%s]",name));
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
