package org.chevron.model;

public interface RegisteredTemplateEngine<T> {

    TemplateEngine<T> getInstance();
    T getConfiguration();

}
