package org.chevron.model;

public class ChevronException extends RuntimeException {

    public ChevronException(String message){
        super(message);
    }

    public ChevronException(String message, Throwable cause){
        super(message,cause);
    }

}
