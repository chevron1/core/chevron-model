package org.chevron.model;

public class TemplatesPoolException extends ChevronException {

    public TemplatesPoolException(String message) {
        super(message);
    }

    public TemplatesPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
