package org.chevron.model.schema;


import java.util.Map;


public interface ObjectSchema {

    void validate(Map<String,Object> params) throws ParamsValidationException;
    String VOID = "Void";

}
