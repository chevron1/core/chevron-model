package org.chevron.model.schema;

import org.chevron.model.ObjectSchemasPool;
import org.chevron.model.SchemaNotFoundException;

import java.util.Optional;

public class ObjectSchemaRef {

    private String schemaName;

    public ObjectSchemaRef(String name){
        this.schemaName = name;
    }

    public ObjectSchema resolve(ObjectSchemasPool pool) throws SchemaNotFoundException {
        Optional<ObjectSchema> optionalObjectSchema = pool.getSchema(schemaName);
        if(!optionalObjectSchema.isPresent())
            throw new SchemaNotFoundException(schemaName);
        return optionalObjectSchema.get();
    }

    public String getSchemaName() {
        return schemaName;
    }
}
