package org.chevron.model.schema;

import org.chevron.model.ChevronException;

public class ParamsValidationException extends ChevronException {

    public ParamsValidationException(String message) {
        super(message);
    }

    public ParamsValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
