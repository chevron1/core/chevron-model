package org.chevron.model;

public class TemplateEngineException extends ChevronException {

    public TemplateEngineException(String message) {
        super(message);
    }

    public TemplateEngineException(String message, Throwable cause) {
        super(message, cause);
    }
}
