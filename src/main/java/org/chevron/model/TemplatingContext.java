package org.chevron.model;


import java.util.Map;
import java.util.function.Supplier;
//Poolable
public interface TemplatingContext {

    void next();//Prepares for the next merge
    String merge(String source, Map<String,Object> properties);
    String merge(Supplier<String> sourceSupplier, Map<String,Object> properties);

}
