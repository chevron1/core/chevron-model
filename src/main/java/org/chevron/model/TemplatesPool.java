package org.chevron.model;

import java.util.List;
import java.util.Optional;

public interface TemplatesPool {

    //Remember: the order of the qualifiers matter
    Optional<Template> getTemplate(String name, Format format, List<String> qualifiers);

}
