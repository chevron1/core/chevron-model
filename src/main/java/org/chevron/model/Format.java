package org.chevron.model;

public enum Format {

    TEXT_PLAIN("text/plain","txt"),
    HTML("text/html","html");

    private String mimeType;
    private String fileExtension;

    Format(String mimeType, String fileExtension){
        this.mimeType = mimeType;
        this.fileExtension = fileExtension;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getFileExtension() {
        return fileExtension;
    }
}
