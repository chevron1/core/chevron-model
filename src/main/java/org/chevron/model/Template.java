package org.chevron.model;

import org.chevron.model.schema.ObjectSchemaRef;

import java.util.List;

public interface Template {

    String getName();
    String getLanguage();
    ObjectSchemaRef getSchema();
    List<String> getQualifiers();
    String getSource();
    Format getFormat();

}
