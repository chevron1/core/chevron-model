package org.chevron.model;

public class ObjectSchemasPoolException extends ChevronException {
    
    public ObjectSchemasPoolException(String message) {
        super(message);
    }

    public ObjectSchemasPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
