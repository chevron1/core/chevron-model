package org.chevron.model;

import java.util.Map;
import java.util.Optional;

public interface ChevronContext {

    Map<String,Object> getDefinitions();
    Optional<RegisteredTemplateEngine> getTemplateEngine(String templateLanguage);
    TemplatesPool getTemplatesPool();
    ObjectSchemasPool getObjectSchemasPool();

}
