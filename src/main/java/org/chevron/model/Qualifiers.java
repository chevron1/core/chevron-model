package org.chevron.model;

public final class Qualifiers {

    public static final String EN = "EN";
    public static final String PT = "PT";

    private Qualifiers(){

    }

}
